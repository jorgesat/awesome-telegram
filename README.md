# awesome-telegram
Listado de Canales, Grupos, Bots y Recursos de Telegram

[Novedades](https://gitlab.com/listados/awesome-telegram/commits/master) - Historial de cambios de este listado
<!-- TOC depthFrom:1 -->
* [CANALES](#canales)
  * [Medioambiente / Sostenibilidad](#medioambiente-sostenibilidad)
  * [ONGs / Asociaciones](#ongs-asociaciones)
  * [Noticias](#noticias)
  * [Informática / Tecnología](#informática-tecnología)
  * [Software Libre / FLOSS](#software-libre-floss)
    * [GNU/Linux](#gnulinux)
    * [Podcasts](#podcasts)
    * [Blogs](#blogs)
    * [Eventos](#eventos)
    * [Distros](#distros)
    * [Empresas](#empresas)
  * [Telegram](#telegram)
    * [Info sobre Bots](#info-sobre-bots)
    * [Directorios / Guías](#directorios-guías)
  * [Seguridad](#seguridad)
  * [Programación / Developers](#programación-developers)
  * [Móviles / Android](#móviles-android)
  * [Cultura/Ocio/Otros](#culturaociootros)
  * [Otros Podcasts](#otros-podcasts)
  * [Política](#política)
  * [Regionales](#regionales)
    * [Región de Murcia](#región-de-murcia)
    * [Comunidad Valenciana](#comunidad-valenciana)
    * [Canarias](#canarias)
    * [Madrid](#madrid)
* [GRUPOS](#grupos)
  * [Software Libre](#software-libre)
  * [Aplicaciones](#aplicaciones)
  * [Podcasts sin canal](#podcasts-sin-canal)
  * [Bash / Shell](#bash-shell)
  * [Distribuciones GNU/Linux](#distribuciones-gnulinux)
    * [Entornos de Escritorio](#entornos-de-escritorio)
  * [Juegos](#juegos)
  * [Android / Moviles](#android-moviles)
  * [Por Regiones](#por-regiones)
* [BOTs](#bots)
  * [Encuestas](#encuestas)
  * [Gestión canales / RRSS](#gestión-canales-rrss)
  * [Administración de Grupos](#administración-de-grupos)
  * [Para encontrar en Telegram](#para-encontrar-en-telegram)
  * [Develop](#develop)
  * [Aprobados por Telegram](#aprobados-por-telegram)
  * [Utilidades](#utilidades)
* [RECURSOS](#recursos)
* [LEYENDA](#leyenda)
<!-- /TOC -->

## CANALES

### Medioambiente / Sostenibilidad / Ciencia

* [ZeroWaste / Residuo Cero](https://t.me/ResiduoCero) - Información referente a la filosofía de vida Zero Waste y medioambiente - _(1 noticia diaria de media)_
  * Tiene [grupo asociado](https://t.me/ZeroWaste_ResiduoCero) - Grupo para comentar las noticias del canal @ResiduoCero administrado por @ResiduoCeroRM
* [ECO Inteligencia](https://t.me/ecointeligencia) - Capacidad de desarrollar productos y servicios diseñados para que desde su creación hasta el final de su vida útil sean beneficiosos para el Medio Ambiente. [Castellano](ecointeligencia.com) & english
* [BlogSOSTENIBLE - Ecología y más](https://t.me/blogsostenible) - Noticias e ideas de ecología, justicia y política ecológica. La sostenibilidad no es opcional. Máximo 1 mensaje diario (de media).
* [Ecoinventos](https://t.me/ecoinventos) - Reciclaje, Energías renovables, medio ambiente, eficiencia energética y desarrollo sostenible.
* [Reducir](https://t.me/reducir) - Reducir tu basura, reducir las compras de productos que no necesitas, reducir el consumo, reduce tu huella... reducir es sostenibilidad, reducir es ayudar al planeta.
* [Ecología y medio ambiente](https://t.me/ecologia_medioambiente) - Noticias en relación a la ecología y el medio ambiente.
* [Arquitectura sostenible](https://t.me/arquitecturasostenible) - se define como un modo de concebir el diseño arquitectónico, optimizando el uso de los recursos naturales y los sistemas constructivos de manera que se minimice el impacto de los edificios sobre el medio ambiente.
* Energías Renovables
  * [Energías renovables DIY](https://t.me/Energias_renovables_DIY) - Nos gustan las energías renovables, creemos en las energías limpias, generamos nuestra propia energía verde.
  * [Eficiencia energética](https://t.me/eficiencia_energetica) - práctica que tiene como objetivo reducir el consumo de energía. La eficiencia energética es el uso eficiente de la energía.
  * [Futuro Renovable](https://t.me/Futuro_Renovable) - La energía del futuro. Ecología y Energías Renovables. Energía solar, eólica, hidroeléctrica, biomasa, hidrogeno. All information about green energy.
  * [Energía Solar ☀️](https://t.me/solar_energia) - Toda la actualidad de la energía solar en la red.
  * [Vehículos y coches eléctricos](https://t.me/vehiculos_electricos) - All information about electric cars. Información y ventas relacionadas con los vehículos y coches eléctricos.
  * [Avatarenergia](https://t.me/avatarenergia) - _Web sobre energías renovables, sostenibilidad... Tienen podcast con episodios de 3 minutos_
* [Curiositix - Curiosidades](https://t.me/curiositix) - De todo un poco, curiosos de nacimiento. - _Mayormente medioambiente_
* [Cumbre Social por el Clima // Climate Social Summit](https://t.me/cumbresocialclima) - Info channel – COP25 Madrid
* [nibö - niboe.info](https://t.me/niboe) - Divulgación científica responsable e inclusiva.

### ONGs / Asociaciones

* [Ecologistas en Acción](https://t.me/ecologistas) - Ecologistas en Acción. Somos una confederación de grupos ecologistas. Con una estructura asamblearia, formamos parte del ecologismo social.
* [Greenpeace_esp](https://t.me/greenpeace_esp) - Organización ecologista y pacifista, económica y políticamente independiente, no acepta donaciones ni presiones de Gobiernos o empresas
* [UNICEF_es](https://t.me/unicef_es) - Trabajamos para que se cumplan todos los derechos de todos los niños y las niñas. Te unes?
* [FACUA](https://t.me/facua) - Luchamos contra los abusos. Somos FACUA. Tu fuerza.
* [esXRebellion](https://t.me/EsXrebellion) - ESXrebellion: Extinción/Rebelión es una movimiento que a través  de la desobediencia civil, masiva, pacífica y sostenida en el tiempo  presiona a la clase política y busca activar a la sociedad ante el colapso climático y la crisis existencial q viene.
* [Juventud por el Clima - Fridays for Future España](https://t.me/jxclima) - Bienvenidx al canal oficial de Juventud por el Clima!! Por aquí publicaremos nuestras campañas, sentadas, acciones, asambleas...  - _Tiene canal asociado_
  * [fff date planning and polling](https://t.me/fffdateplanning) - This group is ONLY for dateplanning stuff

### Noticias

* [MALDITA.ES](https://t.me/maldita_es) - Periodismo para que no te la cuelen
* [MALDITO BULO](https://t.me/malditobulo) - Verificación en Maldita.es. Desmontamos bulos y desinformaciones. Tenemos un @bulobot de Telegram al que enviarle enlaces e imágenes para ser si están ya verificadas.
* [eldiario.es](https://t.me/eldiarioes) - Sistema de alertas informativas
* [elsaltodiario.com](https://t.me/elsaltodiario) - El canal de alertas informativas de El Salto. Hilamos fino, te molestamos lo justo.
* [ELPAÍS](https://t.me/elpaiscom) - Última hora y alertas de noticias
* [Cuartopoder.es](https://t.me/CuartoPoder) - Cuartopoder.es. Periodismo para el 99%
* [El Confidencial](https://t.me/El_Confidencial) - El diario de los lectores influyentes - _No oficial_
* [El Huffpost](https://t.me/elhuffpost) - Última hora, actualidad, opinión y tendencias en El Huffington Post.
* [Público](https://t.me/publico_es) - Canal oficial de Público
* [El Periódico política](https://t.me/elperiodico) - Toda la información política de El Periódico
* [NoticiasCompartidas](https://t.me/noticiascompartidas) - Las noticias más comentadas/compartidas en redes sociales de El País, El Mundo, ABC y eldiario.
* Revistas
  * [Climática](https://t.me/lmclimatica) - Revista especializada en #CrisisClimática de @RevistaLaMarea.  Reportajes, cultura y #NoticiasClimáticas diarias.
  * [Contra el diluvio](https://t.me/contraeldiluvio) - Canal para mantenerte al día de las últimas entradas y proyectos de Contra el diluvio
  * [Pikara Magazine](https://t.me/pikaramagazine) - Revista que ofrece periodismo y opinión con un enfoque feminista, crítico, transgresor y disfrutón.
  * [El Orden Mundial - EOM](https://t.me/elordenmundialeom) - El Orden Mundial en el Siglo XXI (EOM) es un medio digital de análisis internacional privado e independiente. - `[Parado desde Junio]`

### Informática / Tecnología

* [EntrevistasenDiferido](https://t.me/entrevistaendiferido) - Canal donde se realizan entrevistas a diferentes personas sobre temas relacionados con la informática - _(Tiene grupo asociado)_
* [Xataka](https://t.me/xataka) - Apasionados por la tecnología
* [Genbeta](https://t.me/genbeta) - Software, descargas y novedades. Las mejores aplicaciones web, con los trucos más útiles y toda la información.
* [Wwwhatsnew](https://t.me/wwwhatsnew) - Las mejores noticias de wwwhatsnew.com
* [Microsiervos](https://t.me/microsiervos) - Ciencia, tecnología, internet y mucho más
* [MuyComputer](https://t.me/muycomputer) - Toda la actualidad tecnológica y mucho más, en MuyComputer.com.
* [Tecnoyciencia](https://t.me/tecnoyciencia) - Diario de Información tecnológica y científica
* [Noticias Tecnologia](https://t.me/NoticiasTecnologia) - Noticias Tecnologia, Ciencia y Curiosidades
* [uLinks](https://t.me/ulinks) - Descubrimos tesoros en Internet y los compartimos. Un tentáculo más de https://superpatanegra.com
* [ADSLZone](https://t.me/ADSLzoneNet) - Todas las noticias sobre Internet, Fibra Óptica, tecnología, ofertas, tarifas, operadores ¡y más!
* [MovistarNoticias](https://t.me/MovistarNoticias) - Noticias del blog oficial de Movistar
* [Yes We Tech](https://t.me/geekandtechgirls) - Comunidad que a través del Software Libre acerca la tecnología a mujeres, a la vez que lucha por una sociedad inclusiva.
* [URE](https://t.me/URE_ES) - Unión de Radioaficionados Españoles
* [Comunidad A182](https://t.me/ComunidadA182) - Canal de telefonía y multimedia - _Con enlace a muchos grupos sobre compañías telefónicas._
* Domótica
  * [Domótica En Casa](https://t.me/domoticaencasa) - Toda la información relacionada con la domótica para nuestros hogares
    * Tiene [grupo](https://t.me/GizChinaHomeAssistant) - Grupo público para poder ayudar y ser ayudado a la hora de implementar cualquier sistema de domótica.
  * [Enlaces y Material Domology 2.0](https://t.me/linksdomology) - Enlaces, archivos e info de proyectos sobre Domótica interesantes.
    * Complemento del grupo [@Domology](https://t.me/Domology) - Grupo Dedicado A Aprender Y Colaborar Sobre El Apasionante Mundo De La Domótica
* English
  * [Important stories alert for Hacker News](https://t.me/important_stories_alert_hacknews) - Hacker News stories as soon as they reach 600 points.

### Software Libre / FLOSS

* Software Libre
  * [esLibre - anuncios](https://t.me/esLibre_anuncios) - Sigue la actividad de [http://charla.esLib.re](http://charla.eslib.re)
  * [SoftwareLibre](https://t.me/software_libre) - Contenido en español sobre software libre
  * [feminismo 💜 soft.libre 🗝 tecnología 👩‍💻](https://t.me/feminismo_y_tecnologia) - Un canal (por fin) para mujeres y disidentes de género sobre tecnología y software libre
* Cultura Libre
  * [Wikimedia España](https://t.me/wikimedia_es) - Avisos de eventos y noticias de Wikimedia España - www.wikimedia.es
  * [Liberaturadio info](https://t.me/liberaturadio_canal) - Las últimas novedades de la Red de Radios Comunitarias y Software Libre
  * [OpenStreetMaps](https://wiki.openstreetmap.org/wiki/List_of_OSM_centric_Telegram_accounts) - _Listado con oficial canales y grupos_
    * [OSMF News Español](https://t.me/osmbloges) - Todas las noticias oficiales de la Fundación OpenStreetMap
    * [OSMbot](https://t.me/OSMbot_channel) - News and tips for @OSMbot, the OpenStreetMap bot for Telegram.
* Makers
  * [CV19Makers_Anuncios](https://t.me/CV19Makers_Anuncios) - Canal para centralizar la información de la iniciativa colaborativa de ayuda a la sanidad
* Aplicaciones
  * [Facturascrips 2020](https://t.me/facturascripts) - _Video-tutoriales sobre caracteristicas de la aplicación_
* FLOSS
  * [Free Software Foundation](https://t.me/FreeSoftwareFoundation) - Unofficial FSF Channel. Here you'll find all sorts of news regarding free software
    * Grupo asociado [StallmanWasRight](https://t.me/stallmanwasright) - _(mucho tráfico, discuten de política en inglés)_
  * [It's FOSS](https://t.me/itsfoss_official) - Official telegram channel of It's FOSS. Join to get the latest from Linux and Open Source world. itsfoss.com
  * [Mastodon Project](https://t.me/mastodonnews) - Read all news from the official joinmastodon.org blog and much more.
  * [Libreware](https://t.me/libreware) - English - _Todo tipo de aplicaciones libres_

#### GNU/Linux

* [Un Día Una Aplicación](https://t.me/UnDiaUnaAplicacion) - Cada día se mostrará una aplicación de Linux.
* [Phoronix News](https://t.me/Phoronix) - Phoronix News. - _Benchmarks y reviews en inglés_
* [Linuxgram 🐧](https://t.me/linuxgram) - News and info from the Linux world 🐧
* [omg! ubuntu!](https://t.me/omgubuntu) - Get notified of the latest posts, what we're planning, and how you can help.
* [Command Line Magic](https://t.me/climagic) - Cool Unix/Linux Command Line tricks you can use in \~140 characters
* [Nación Linux](https://t.me/nacion_linux) - Clound general con articulos,links y archivos
* [LiGNUx](https://t.me/LiGNUxNews) - Canal de noticias LiGNUx
  * Tiene [grupo](https://t.me/lignux)
* [LinuxBloc](https://t.me/LinuxBloc) - Anotaciones , enlaces , programas y ajustes para GNU/Linux
* [GNU Propaganda](https://t.me/GNUPropaganda) - Distribute these to help others learn about the evils of proprietary software, and the benefits of free software\! CC-BY-SA
* [Linux Administration](https://t.me/LinuxAdministration) - `[Parado desde 2019-06]`

#### Podcasts

* [Compilando Podcast](https://t.me/compilandopodcast) - GNU/Linux y software libre
* [Podcast Linux](https://t.me/podcastlinux) - Canal de Telegram para Podcast Linux de @JuanFebles. Noticias + Audios de Linux Express
* [Canal atareao](https://t.me/canal_atareao) - _(Tiene grupo asociado)_
* [GNU/Linux Valencia](https://t.me/canalgnulinuxvalencia) - Nuestro objetivo es la difusión de GNU/Linux. Se pretenden hacer reuniones mensuales para realizar desde instalaciones en equipos, resolver problemas, mostrar facilidad de uso, funcionamiento, etc...
  * Tienen [grupo](https://t.me/gnulinuxvalencia) -
* [El Podcast de Eduardo](https://t.me/elpodcast) - [Podcast](https://www.eduardocollado.com) sobre Redes, Linux, Hosting .... y directos.
  * Grupo asociado [Grupo Podcast](https://t.me/grupopodcast) para comentar los capítulos - _(+ de todo un poco)_
  * Canal asociado [Audios - El Podcast de Eduardo](https://t.me/elpodcastaudios) - Audios del podcast
* [Ochobitshacenunbyte.com](https://t.me/ochobitshacenunbyte) - Canal de los seguidores de la [web](https://www.ochobitshacenunbyte.com)
* [OpenExpo Europe Channel](https://t.me/OpenExpo) - Noticias de OpenExpo Europe y Más Allá De La Innovación(Podcast)
  * [Grupo asociado](https://t.me/OpenExpoEurope) - Web del [evento](https://openexpoeurope.com/es/) empresarial que publica el podcast ["Más allá de la innovación"](https://masalladelainnovacion.com)
  * [uGeek](https://t.me/uGeek) - Toda la actualidad de Linux, Android, Raspberry Pi, impresoras 3D…
    * Asociado [Grupo del Podcast uGeek](https://t.me/uGeekPodcast)
* [Canal Salmorejo Geek](https://t.me/salmorejogeek) - Batiburrillo Digital
* [Mosqueteroweb](https://t.me/mosqueterowebencanal) - Para publicar los contenidos creados por mi: podcasts, artículos, promociones, sorteos y otros.
* [TomandoUnCafe](https://t.me/TomandoUnCafe) - Podcast sobre aplicaciones de Linux que han aparecido en el canal @UnDiaUnaAplicacion y reflexiones u opiniones sobre diversos temas relacionados con la informática. - _Tiene grupo privado_
* [Aviones De Papel](https://t.me/avionesdepapel) - Canal del Podcast Aviones de Papel - _Tiene grupo privado_
* [ARM para TODOS](https://t.me/ARMparaTodos) - Canal donde están los audios del podcast ARM para Todos - _Tiene grupo privado compartido con Tomando un Cafe_
  * [ARM(alternativas a Raspberry)](https://t.me/AlternativasRaspberry) - Grupo sobre alternativas a Raspberry, donde hablaremos de marcas como Pine64,OrangePi,NanoPI, BananaPi,LibreComputer y otras marcas.
* [NeoSiteLinux - Software Libre y más...](https://t.me/neositelinux) - Canal de Telegram de www.neositelinux.com
  * [NeoSiteLinux Podcast (Audios)](https://t.me/neositelinuxpodcast) - Los Audios de NeoSiteLinux.com alojados en Telegram
  * Grupo asociado [NeoSiteLinux (grupo)](https://t.me/neositelinuxgrupo)
* [Podcast Inside](https://t.me/systeminsideaudio) - Todos los Podcast de SystemInside. [https://tupodcast.com](https://tupodcast.com) (Parado desde 2019-03)
* [Maratón Linuxero](https://t.me/maraton_linuxero) - Proyecto de emisión en vivo de [podcasts linuxeros](http://maratonlinuxero.org/)
  * Tiene [grupo](https://t.me/maratonlinuxero) - Emisiones podcasteras sobre software libre.
* [GDF Estudio](https://t.me/gdfestudio) - Entérate de en qué se está trabajando en GDF Estudio antes que nadie. Magia y Tecnología. Podcast Rebasando el Horizonte
* [Killall Radio Podcast Telegram](https://t.me/killallradiopodcast) - Audios colaborativos del Killall Radio Team. - `[Parado desde 2019-05]`
* Programación
  * [Web Reactiva](https://t.me/webreactiva) - enlaces a los nuevos episodios de [Web Reactiva](https://www.danielprimo.io/podcast) / historias que tienen que ver con la programación y el desarrollo web
  * [El podcast de JustDevZero](https://t.me/podcastjustdevzero) - Canal dedicado a la distribución de mi [podcast](https://podcast.justdevzero.me/) homónimo.

#### Blogs

* [MuyLinux](https://t.me/muylinux) - Linux, FLOSS y mucho más, en [https://MuyLinux.com](https://muylinux.com)
* [Colaboratorio](https://t.me/colaboratorio) - Nanomedios y experiencias virtuales
* [SoloLinux](https://t.me/sololinux_es) - Telegram oficial de sololinux.es
* [LaMiradaDelReplicante](https://t.me/lamiradadelreplicante) - Blog sobre Linux y Software Libre - _Calidad frente a cantidad_
* [_voidNull](https://t.me/voidnullblog) - Canal Oficial del blog [_ voidNull](http://www.voidnull.es)
* [Desde Linux](https://t.me/desdelinuxweb) - Linux en estado puro. Las noticias de última hora y tutoriales más variados y completos.
* [Linux Adictos](https://t.me/linuxadictos) - Linux para auténticos adictos y fans. Noticias cada día y los tutoriales más completos que podrás encontrar.
* [masLinuXBlog](https://t.me/canalmaslinuxblog) - Canal sólo artículos del blog - Tiene [Grupo](https://t.me/MasGNULinuXBlog)
* [Emezeta](https://t.me/Emezeta) -Blog [https://www.emezeta.com/](https://www.emezeta.com/) - `[Sin contenido]`

#### Eventos

* [OpenExpo Europe Channel](https://t.me/OpenExpo) - Noticias de OpenExpo Europe y Más Allá De La Innovación(Podcast)
  * Tiene [grupo asociado](https://t.me/OpenExpoEurope) - Web del [evento](https://openexpoeurope.com/es/) empresarial que publica el podcast ["Más allá de la innovación"](https://masalladelainnovacion.com)
* [lyt19](https://t.me/lyt16) - Linux y Tapas en **León** capital.
  * Tiene [grupo](https://t.me/linuxytapas)
* [Opensouthcode](https://t.me/opensouthcode) - Evento software libre y opensource. **Málaga** - `[Parado]`

#### Distros

* [openSUSE News](https://t.me/opensusenews) - News from https://news.opensuse.org/ - _Oficial_
* [Fedora Project News](https://t.me/fedoranews) - Official news relating to the Fedora Project (getfedora.org). Managed by Fedora Marketing and CommOps teams.
* [EndeavourOS-news](https://t.me/endeavour_news) - community driven Antergos fork
* Arch Linux
  * [Planet Arch Linux & News](https://t.me/planetarch) - Planet Arch Linux is a window into the world, work and lives of Arch Linux hackers and developers. Also we have the latest news from the Arch Linux development staff.
  * [Arch Linux: Recent package updates](https://t.me/archlinux_updates) - Channel with recent package updates in Arch Linux repositories.
  * [Arch Linux News](https://t.me/archlinuxnews) - Latest news from Arch web site. - `[Parado desde 2018-02]`
  * [Planet Arch](https://t.me/archplanet) - Planet Arch Linux latest posts. - `[Parado desde 2018-02]`
* [MiniNo](https://t.me/galponminino) - Para ejecutarse en equipos con pocos recursos hardware - `[Abandonado?]`
* Entornos de escritorio
  * [Kdeblog](https://t.me/kdeblog) - blog sobre el Software Libre centrado en la Comunidad KDE - _Retweets de su cuenta de Twitter_

#### Empresas

* [Slimbook](https://t.me/slimbook) - Slimbook TEAM news. Portátiles y equipos de gama alta con Linux preinstalado desde Valencia
* [VANT](https://t.me/vantpc) - Portátiles y sobremesas linuxeros, muy linuxeros desde Valencia
* [JuncoTIC - juncotic.com](https://t.me/juncotic) - Cursos, capacitaciones y consultoría en tecnologías libres - _Cursos online y presenciales_
* [Amalia López Acera RRSS AAPP MarcaPersonal](https://t.me/amalialopezacera) - Canal con información y recursos sobre redes sociales para personas que trabajan en las administraciones públicas

### Telegram

* Oficiales
  * [Telegram News](https://t.me/telegram) - The official Telegram on Telegram. Much recursion. Very Telegram. Wow.
    * [Telegram en español](https://t.me/TelegramES) - Canal oficial de Telegram en español
  * [Telegram Desktop](https://t.me/desktop) - The official Telegram Desktop channel, firsthand information from the developer.
  * [Durov's Channel](https://telegram.me/durov) - This is where I post thoughts about Telegram in a slightly less formal and more direct way than in the official Telegram blog. - _El fundador de Telegram_
  * [Telegram X](https://t.me/tgx_android) - Developer's channel - Telegram X
  * [Telegram Designers](https://t.me/designers) - Feature suggestions for Telegram from designers all over the world. If you want to contribute, send your UI mockups to @design_bot
  * [Telegram Contests](https://t.me/contest) - Here we announce Telegram coding contests in Android Java, iOS Swift, JS, C/C++. Discussion: @contests
* [Open Source Telegram](https://t.me/OpenSourceTelegram) - Is Telegram Open Source yet?
* [Privacy Today](https://t.me/privacytoday) - All things privacy, open source, libre philosophy and more! - _Aspectos de privacidad en Telegram_
  * Tiene [grupo](https://t.me/PTuring) - _Para entrar en el grupo real hay que leer el mensaje de bienvenida,no ser un bot y saber inglés. OJO Muchísimos mensajes_
* [Más➕TELEGRAM👑🚀](https://t.me/mastelegram) - Toda la información para que expriman al 100 esta aplicación: consejos, tips, betas, estables y mucho más. - _(Tiene grupo asociado)_
* [Stickers Animados](https://t.me/StickersAnimados) - Los mejores Stickers Animados de Telegram aquí
* [Trending Stickers](https://t.me/TrendingStickers) - _Nuevos Stickers con info y su autor_
* [Telegram Geeks](https://t.me/geekschannel) - 🚀 Join the Telegram Army\!
* [Red Telegram ✅ Canales, Grupos y Bots](https://t.me/RedTelegram) - Guía de Canales, Grupos y Bots recomendados en Telegram.
  * [Noticias Telegram](https://t.me/NoticiasTelegram) - Avisos y novedades de Telegram en Español
  * [Guia de Canales Telegram](https://t.me/GuiaDeCanales) - Los Canales mas recomendados de cada tematica.
  * [Guia de Grupos Telegram](https://t.me/GuiaDeGrupos) - Los Grupos mas recomendados de cada tematica.
  * [BOTLIST](https://t.me/botlist) - •Los mejores bots de Telegram en una lista•
* [Telegram Info En](https://t.me/tginfoen) - English mirror of @tginfo
* Aplicaciones
  * [Telegram Android ID Channel](https://t.me/tgaidchannel) - All about related #Telegram #collections & #apps #android [ #official, #unofficial, #mod, #review, #news, etc ]
  * [Telegram Beta](https://t.me/tgbeta) - _Información sobre actualizaciones en Android e iOS principalmente_
    * Tiene [grupo asociado](https://t.me/tgbetachat) - Telegram news, bugs, and features. _El desarrollador de Telegram para Android esta en este grupo_
    * [tgfiles](https://t.me/tgfiles) - _Todos los ficheros de las aplicaciones moviles y de escritorio_
  * [Plus Messenger oficial](https://t.me/PlusMsgrES) - Canal oficial de Plus Messenger en español
    * Tiene [grupo](https://t.me/TodoSobrePlusMessenger) oficial
* Parados
  * [Telegram Channels](https://t.me/TlgrmChannels) - `[Parado 2019-02]`
  * [Variedades Telegram](https://t.me/VariedadTelegram) - Variedades Telegram - `[Parado 2017]`

#### Info sobre Bots

* [BotNews](https://t.me/BotNews) - The ***official*** source for news about the Telegram Bot API.
* [Bot Updates](https://t.me/BotUpdates) - The team that makes life better with using bots.
* [Controller Updates](https://t.me/ControllerUpdates) - Status updates and small announcements related to @ControllerBot
* [@MiTrackingBot - Info y novedades](https://t.me/MiTracking) - MiTrackingBot - Info y novedades de @MiTrackingbot: en este canal se irá añadiendo información relevante sobre el bot y todas sus mejoras. También puedes debatir en @MiTrackingChat
* [WJClub News](https://t.me/WJClubNews) - Official News Channel of @WJClub (Noticias sobre algunos bots de encuestas) - `[Parado 2019-07]`
* [Bots Channel](https://t.me/BotsChannel) - `[Parado 2018-06]`

#### Directorios / Guías

* [DIRECTORIO TELEGRAM](https://t.me/DirectorioTelegram) - AMPLIA LISTA DE CANALES, GRUPOS, BOTS DE FORMA ORDENADA Y POR CATEGORÍAS.
* [Telegrias (Guia en Telegram)](https://t.me/Telegrias) - similar a una TV Guia donde encontrara grupos, canales y paginas, referentes a un cierto contenido y acomodados por categorias y etiquetas,
* [Tus canales telegram](https://t.me/tuscanalestelegram) -
* [Índice Telegram](https://t.me/indicetelegram) - Este canal es un índice no oficial de Telegram
* [Telegram Channels](https://t.me/tchannels) - Get to know the most interesting new Telegram channels\! tchannels.me (Parado desde 2017-12)
* [GameeApp](https://t.me/gameeapp) - Unofficial Complete list of games by @gamee. Requires Telegram v 3.13

### Seguridad

* [Una al día](https://t.me/unaaldia) - Canal de Telegram con las noticias diarias de [UAD.](http://unaaldia.hispasec.com/)
* [CyberSecurityPulse](https://t.me/cybersecuritypulse) - Canal de noticias y reflexiones sobre ciberseguridad del equipo de innovación y laboratorio de ElevenPaths
* [Elladodelmal](https://t.me/Elladodelmal) - Posts de "Un informático en el lado del mal" a través de Telegram
* [Derecho de la Red](https://t.me/DerechodelaRed) - Ciberseguridad, Privacidad, Redes Sociales, Derecho de las TIC y mucho más. Todo con un toque jurista...
* GINSEG
  * [Cyber Threat Intel News](https://t.me/ThreatIntelligence) - Canal de noticias de la comunidad [GINSEG](https://t.me/ginseg)
    * [Grupo Analisis de inteligencia español](https://t.me/Analisisdeinteligencia) - Aportar material y sucesos interesantes encuadrado en Análisis de Inteligencia y para Analistas. Temática en Español.
  * [Domain Squatting / DNS Security](https://t.me/DNSSecurity) - Grupo enfocado en estas amenazas: #CyberSquatting #TypoSquatting #IDN #Dominios #DNS
* [Inteligencia más Liderazgo](https://t.me/inteligenciayliderazgo) - Formación y consultoría en inteligencia
* [Ciberpatrulla - Técnicas OSINT](https://t.me/ciberpatrullacom) - Ciberpatrulla - Técnicas OSINT
* [🔒Seguridad Informática](https://t.me/seguridadinformatic4) - Compartimos conocimientos en seguridad informática, aunque el término más indicado debería ser autodefensa digital.
* [Securizando](https://t.me/Securizando) - Canal con información de seguridad informática
  * Tiene [grupo](https://t.me/joinchat/Df1auEQjwVQPsir1hMMqqg)
* [Security News for Everyone](https://t.me/SeguridadInformatica) - Canal dedicado a noticias de Seguridad Informática en todos sus ámbitos, gratuito y sin publicidad 24h x 365 días al año
* [Pléyades IT - Ciberseguridad](https://t.me/pleyades_it) - Canal de 📰 actualidad y 🚨 alertas de Ciberseguridad.
  * Tiene [grupo](https://t.me/pleyades_it_grupo) - Grupo de ciberseguridad, informática y tecnología en general. Objetivo: compartir conocimientos, opiniones y resolver dudas.
* [Segu-Info Channel](https://t.me/SeguInfoChannel) - Canal de Seguridad de la Información de Segu-Info. Más info: https://blog.segu-info.com.ar
* [Flu Project](https://t.me/fluproject) - Canal oficial de la comunidad de habla hispana Flu Project. royecto nacido en 2010 con el objetivo de divulgar la ciberseguridad.
* [Team Whoami](https://t.me/teamwhoami) - Canal dedicado a la seguridad informatica
* [Hacking tools y tal](https://t.me/hackingtools) - Recopilación de enlaces interesantes sobre temas de ciberseguridad
* Derechos Digitales
  * [internautas](https://t.me/asociacion) - Asociación de Internautas
  * [Xnet - Info al momento de nuestras acciones](https://t.me/XnetInfo) - Democracia en la era digital. Tecnopolítica. Lucha contra la corrupción. Información y cultura libres. - _Miembro de EDRi (European Digital Rights)_
  * [🐾AutoDefensaDigital🐾](https://t.me/AutoDefensaDigitalenLucha) - Decir que no te preocupa la privacidad porque no tienes nada que esconder es como decir que no te importa la libertad de expresión porque no tienes nada que opinar
  * [Interferencias Canal](https://t.me/inter_ferencias_ruido) - Asociación ciberactivista para compartir actividades, información y opinión sobre #DerechosDigitales, #SeguridadInformatica y similares.
    * Tiene [grupo](https://t.me/inter_ferencias) - Sin software libre no hay privacidad posible. _Granada_
* En inglés
  * [SysAdmin 24x7](https://t.me/sysadmin24x7) - Noticias y alertas de seguridad informática.
  * [Cyber Security News](https://t.me/Cyber_Security_Channel) - Cyber Security Breaking News.
  * [ALL ABOUT HACKER](https://t.me/ALLABOUTHACK) - https://www.allabouthack.com - Tiene [grupo](https://t.me/AllAboutHack2)
* ~~[Seguridad Informática - Redbyte & CeroHacking](https://t.me/REALHACKERS) - APRENDE TODO SOBRE SEGURIDAD INFORMÁTICA Y HACKING~~

### Programación / Developers

* [Programmer Jokes](https://t.me/programmerjokes) - This channel will serve you daily programmer jokes. Feel free to laugh :)
* [The Devs](https://t.me/thedevs) - Developers Community on Telegram.
* [Opensource Findings](https://t.me/opensource_findings) - Links and concise reviews on open-source tools, news, and talks about language-design, trends and fundamentals.
* [UnPythonAlDia](https://t.me/UnPythonAlDia) - Cada día se mostrará una herramienta de desarrollo para Python.
  * [AprendePython](https://t.me/aprendepython) - Tutoriales, artículos y cursos sobre el ecosistema de Python.
* [Programar Fácil](https://t.me/programarfacilc) - #Arduino no es la solución para todo pero si que es el comienzo para buscar esa solución #Maker #DIY #SAV https://campus.programarfacil.com/
* [Desarrolladores Informaticos](https://t.me/DesarrolladoresInformaticos) - Programación , Hacking , Inteligencia Artificial , Ciberseguridad y todo lo relacionado con la Informática. !!!
  * Tiene [grupo](https://t.me/Desarrolladores_Informaticos) - compartir material y conocimiento
* SEO
  * [SEO Blogging y Marketing digital](https://t.me/seo30) - Canal sobre SEO y marketing online.

### Móviles / Android

* [El Androide Libre](https://t.me/AndroideLibre) - Noticias Android
* [Andro4all](https://t.me/andro4all) - Android, noticias de tecnología, análisis, comparativas, ofertas... ¡Toda la actualidad móvil al alcance de tu mano!
* [MovilZona](https://t.me/MovilZonaES) - Reviews, tutoriales, curiosidades, noticias y opinión en tu canal de tecnología.
* En inglés
  * [Fairphone Blog](https://t.me/fairphoneblog) - Unofficial Fairphone Blog Channel
  * [UBports News Channel](https://t.me/ubports_news) - The very latest from the UBports Community\!
  * [LineageOS ](https://t.me/LineageOS) - `[Parado desde 2019-03]`
    * Tiene [grupo asociado](https://t.me/LineageOS_group)
    * Para [offtopic](https://t.me/lineageos_offtopic)
  * [/e/ announcements](https://t.me/mydataismydata) - Latest news from /e/ - my data is MY data https://e.foundation

### Cultura/Ocio/Otros

* [Planeta DIY - Hazlo tu mismo](https://t.me/DIY_Hazlotumismo)
* Ciencia
  * [Nibö](https://t.me/niboe) - Divulgación científica crítica, responsable e inclusiva.
  * [Círculo Escéptico](https://t.me/circuloesceptico) - Canal público con noticias del mundo del pensamiento crítico y el escepticismo.
  * [Cienciamania](https://t.me/cienciamania) - Canal que recopila los post de los mejores blogs y páginas de ciencia de la web en español
  * [Astrocurioso](https://t.me/Astrocurioso) - Un canal donde encontrarás curiosidades, noticias y todo los relacionado con está hermosa rama de la ciencia.
  * [BIOLOGÍA Y NATURALEZA](https://t.me/Biologia_Naturaleza) - Artículos, Fichas, Notas, Infografías, Imágenes, Videos y Documentales relacionados con las ciencias de la naturaleza y en especial la biología y sus ramas.
  * [MARIPOSAS IBÉRICAS](https://t.me/mariposasibericas) - Mariposas de la Península Ibérica
* Conservacion ambiental
  * [OBSERVATORIO DEL ESTADO DE CONSERVACIÓN DEL LOBO IBÉRICO](https://t.me/observatorioloboiberico) - Canal oficial OBSERVATORIO DEL ESTADO DE CONSERVACIÓN DEL LOBO IBÉRICO.
  * [Fapas](https://t.me/fapas_spain) - Canal de alertas de noticias generadas por el Fondo para la Protección de los Animales Salvajes (FAPAS).
  * [Rewilding Wild–Europe](https://t.me/rewilding) - Canal de comunicación impulsado por B.Varillas para los interesados en seguir la información de las reservas de Rewilding en Iberia.
* Educación
  * [EduCanales](https://t.me/Educanalgrupos) - Para recopilar canales y grupos  relacionados con el mundo educativo
  * [Educación Especial - Material](https://t.me/EducacionEspecial) - Canal donde se comparte recursos que facilitan el proceso de enseñanza y aprendizaje en Educación Especial
  * [EDUCACIÓN](https://t.me/educacion) - Educación Digital con lo último.
  * [EDUCACIÓN 3.0](https://t.me/EDUCACION3_0) - EDUCACIÓN 3.0 es el medio de comunicación líder en innovación educativa, nuevas tecnologías y metodologías, innovación docente, formación y recursos para profesores.
* Salud
  * [Urgencias y emergencias](https://t.me/urgenciasyemergencias) - Información, infografías, guías y más
  * [Salud en gotas💧](https://t.me/saludengotas) - Tips&Facts de salud con rigor científico y en castellano.Tratamos de basar todo lo que decimos en estudios bien realizados y replicados(con alto nivel de evidencia)
  * [Informe COVID-19 (España)](https://t.me/covid19_spa) - Cada 2 horas, informe del estado de la infección de SARS-CoV-2 (COVID-19) en España
* Alimentación
   * [DSP - Dietética Sin Patrocinadores](https://t.me/dieteticasinpatrocinadores) - Canal de noticias de Dietética Sin Patrocinadores. Más info en @dieteticasin en Twitter, Facebook e Instagram. - `[Parado desde 2019-02]`
   * [Realfooding](https://t.me/realfooder) - Realfooding por Telegram. Quédate para saber todas las novedades.
   * [Recetas veganas 😋](https://t.me/recetas_veganas) - Recetas veganas de sana y deliciosa comida
   * [Canal Veggie](https://t.me/recetasveggiesconamor) - Canal de recetas vegetarianas y veganas y más! - _Tiene grupo privado asociado_
   * [Vegan & Vegetarian](https://t.me/vegan) - Infografías y fotos, [Abandonado desde 2016], pero tiene material interesante.
 * Huertos
   * [Cultiva Tu Huerto](https://t.me/cultivatuhuerto) - Aprende a cultivar tus hortalizas de forma sencilla en casa en tu huerto urbano o maceta
   * [Huerto ecológico](https://t.me/Huerto_ecologico) - Producir para autoconsumir. Aprende a cultivar tu propio huerto ecológico.
* Televisión
  * [Órbita Laika](https://t.me/orbitalaika) - Programa  de divulgación científica y humor  emitido por televisión española.
  * [El Cazador de Cerebros](https://t.me/elcazadordecerebros) - Programa de TVE, donde Pere Estupinyà buscará las mentes más brillantes de la actualidad para que nos contagien su sabiduría.
  * [Frikimalismo FM 2.0](https://t.me/frikimalismoFM) - ¿Eres un friki? ¿No lo eres? Frikimalismo FM es tu podcast. Cine, series, ciencia, entrevistas y lo que surja… Con @lalachus3, @monsuarez, @acevedismos, @domingo_montoya y @colesterio
  * [FormulaTV - Television Noticias](https://t.me/FormulaTV) - Noticias Television📺
  * [Noticias Movistar+ - Television Pago](https://t.me/MovistarPlusNovedades) - Novedades del servicio Movistar+ y canales de 📺television de pago💶
* Historia
  * [Historia en cápsulas](https://t.me/capsulahistoria) - Canal de fotografías y anécdotas históricas
  * [Carrusel por la Historia](https://t.me/HistoriayArqueologia) - Novedades históricas y artículos histórico-paleontológicos
* Arte / Dibujo
  * [Devir ✅ .](https://t.me/jude_devir) - Devir ✅ 💢<ORIGINAL CHANEL>💢 🔰Maya and me have some story every week and share with you ^^ 🔰So we're enjoy to join us. - _(Tira cómica de pareja)_
  * [Art planet](https://t.me/artplanet) - Art planet
  * [World Architecture](https://t.me/Architecture_World) - Architectural structures from around the world
  * [Nature 🌄](https://t.me/naturus) - Best photos of nature all over the world!
* Música
  * [Trascendencia iRadio Show](https://t.me/TrascendenciaVe) - Podcast y Noticias del Rock & Metal Mundial
  * [Ana Guerra](https://t.me/anaguerrawarmusic) - _Canal personal?_
* Literatura
  * [Frases | Literatura | Aforismos](https://t.me/Frases_Literatura_citas) - Aquí encontrarás las mejores frases de los mejores escritores, poetas y artistas
  * [Carmen Ibarlucea](https://t.me/Ibarlucea) - Información sobre actividades de la narradora oral y escritora ecofeminista antiespecista
* Electrónica y Mecánica
  * [Mecatrónica AE ⚙️](https://t.me/electronicamecanica) - Canal dedicado a la información breve y clara sobre el maravilloso mundo de la mecatrónica
* [Legales Sin Fronteras](https://t.me/legalessinfronteras) - Cooperativa Jurídica sin ánimo de Lucro Social y Solidaria que pretende dar información gratuita a todas las personas.
* [🚺💜🚴La bicicleta morada](https://t.me/labicicletamorada) - "la bicicleta ha hecho más por la mujer que ninguna otra cosa". women empowerment, traveling, sustainable mobility, urbanism, news, events.
* [Que pasa con tus Bichitos](https://t.me/ConoceTusBichos) - Mini Documentales de Animales

### Otros Podcasts

* Tecnología
  * [NASeros Podcast](https://t.me/naserospodcast) - Sigue los podcast de Naseros.com en Telegram
    * Tiene [grupo oficial](https://t.me/NASeros) - NAS, redes, almacenamiento, multimedia y seguridad.
  * [Republica Web](https://t.me/republicaweb) - dirigido a todas las personas que se ganan la vida con la web. Hablamos con libertad sobre cuestiones relacionadas con internet y nuestra profesión de formadores y desarrolladores web. _Tiene grupo [privado](https://republicaweb.es/)_
  * [eDucando Geek](https://t.me/educandogeek) - encontrarás refencias a mi experiencia de uso con diferentes dispositivos, ordenadores, etc. - _Solo audios_
  * [Órbita friki](https://t.me/orbitafrikinews) - Podcast donde @igorregidor y @tescaso hablan sobre libros, comics, series, peliculas, juegos o hacen entrevistas a representantes de la comunidad friki.
    * Tiene [grupo](https://t.me/orbitafriki) - Descubre, disfruta y comparte esas aficiones que vives con pasión. Podcast Orbita Friki (antes Reto Friki)
  * [Ingenieria Inversa](https://t.me/ingenieriainversa) - Tecnología y Retroinformatica
  * [Cosas de ModernOS](https://t.me/cosasdemodernoscanal) - Un podcast donde tres viejóvenes cuentan sus vivencias tecnológicas de abuelos cebolletas y recuerdos de doña Concha Velasco.  @lareddemario @yoyo308 @josescolar
    * Tiene [grupo](https://t.me/cosasdemodernos)
  * [Tiempo escaso](https://t.me/tiempoescaso) - El canal sobre Pebble y otros wearables
  * [Secure Podcast - Channel](https://t.me/securepodcast) - Un intento de Podcast sobre InfoSec, hecho por y para la Comunidad. - _Retweets de su cuenta de Twitter_
* [La filosofía no sirve para nada](https://t.me/filosofianada) - es un podcast sin pretensiones en el que reflexionaremos sobre el presente.
  * Tiene [grupo](https://t.me/opinafilosofianada)
* [Al otro lado del micrófono](https://t.me/alotroladodelmicrofono) - noticias, eventos, herramientas, curiosidades y recomendaciones relacionadas con el podcasting.
* [Porqué podcast](https://t.me/porquepodcast) - navega cada mes hacia rumbo desconocido para regresar cada día 15 al mismo puerto
* [Hablemos de Montessori](https://t.me/hablemosdemontessori) - dedicado a la Educación #Montessori
* [Red de podcast Nación Podcaster](https://t.me/nacionpodcast)
  * Tiene [grupo](https://t.me/nacionpodcaster)
* [Carne Cruda](https://t.me/carnecruda) - Recibe nuestros mensajes de texto y voz con las últimas noticias y contenidos. - _Programa de radio_
* [Va Por Nosotras](https://t.me/vapornosotraspodcast) - Salud, sexualidad, entretenimiento, deporte, relaciones, familia, emprendimiento o moda y belleza.Fomentar la igualdad es la base de nuestros contenidos.
  * Tiene [grupo](https://t.me/vapornosotras) - Entrevistamos a mujeres que tienen una historia que contar, mujeres que quieren comunicar sus logros, inquietudes y compartir sus experiencias.
* [Viajo en Moto](https://t.me/viajoenmoto)
  * Tiene [grupo](https://t.me/chat_viajo_en_moto)
* [Historacing](https://t.me/historacing)
* [Casus Belli Podcast](https://t.me/casusbellipodcast) - ¡Vive la Historia Bélica del Siglo XX! Programa de Pódcast.
* Ciencia
  * [Carreras Científicas Alternativas](https://t.me/carrerascientificasalternativas) - [Plataforma](https://carrerascientificasalternativas.com/) de guía para científicos e investigadores interesados en redirigir su carrera más allá del mundo académico 🎓🚸
  * [GeoCastAway](https://t.me/geocastaway) - _Geología_
    * Tiene [grupo](https://t.me/geocastawaypodcast)
* En inglés
  * [The minimalists](https://t.me/theminimalistspodcast)- Live a [meaningful](https://www.theminimalists.com/) life with less🍃🗻🌾 _Como vivir con menos, siendo más sostenible_

### Política

* [1906 news](https://t.me/news1906) - Encuestas y estudios de opinión. Procesos electorales.
* Partidos
  * [PODEMOS](https://t.me/ahorapodemos) - Canal de Telegram oficial de PODEMOS.
    * [En el Exterior Podemos](https://t.me/podemosexterior) - círculos en el exterior
    * [Contigo en las administraciones - PODEMOS](https://t.me/PodemosAdmonPublica) - Canal oficial de la Secretaria de Administraciones Públicas de PODEMOS
  * [PSOE](https://t.me/canalPSOE) - Canal oficial del PSOE en Telegram
  * [Más País](https://t.me/MasPais_Es) - Nacemos para ser el antídoto a la abstención y sacar a nuestro país del bloqueo. Por un país más verde, feminista, justo y libre. Súmate\!
  * [Ciudadanos Cs](https://t.me/CsCiudadanos) - Perfil oficial de Ciudadanos. Somos un partido liberal progresista, demócrata y constitucionalista.
  * [Partido Popular](https://t.me/Partido_Popular) - Partido Popular Perfil Oficial
  * [Izquierda Unida🔻](https://t.me/iunida) - Canal oficial de Izquierda Unida en Telegram. #PorUnNuevoPaís más justo, digno, democrático y soberano.
    * [Animalistas IU](https://t.me/AnimalistasIU) - Canal oficial de comunicación del Movimiento Animalista de IU, Área Federal de Izquierda Unida que trabaja por los Derechos Animales.
  * [PACMA](https://t.me/PartidoPACMA) - Todas las campañas y acciones del único partido en España que defiende sin excusas los derechos de los animales, el medioambiente y la justicia social.
  * [☠️ PIRATAS.org ☠️](https://t.me/PiratasORG) - Canal del Movimiento Pirata en el Estado español ☠️
* Congreso de los Diputados (Madrid)
  * [Congreso de los Diputados](https://t.me/CongresodelosDiputados) - Canal para estar al tanto de la actualidad del Congreso
    * [BOE diario](https://t.me/BOEdiario) - Titulares diarios (de lunes a sábado) del Boletín Oficial del Estado.
  * [PP Congreso](https://t.me/GPPopular) - en el Congreso de los Diputados
  * [Podemos Congreso](phttps://t.me/podemoscongreso) - Toda la actualidad de PODEMOS en el Congreso - `[Parado desde 2018]`
* Sindicatos
  * [Canal CCOO](https://t.me/canalccoo) - Canal de la Confederación Sindical de Comisiones Obreras
  * [UGT Comunicación](https://t.me/ugt_es) - Canal de noticias de la Unión General de Trabajadores
  * [CNT.es](https://t.me/CNTes) - Canal oficial del sindicato CNT (Confederación Nacional del Trabajo) Este canal depende de la Secretaría de Comunicación de CNT
* Pastafaris
  * [Pastafaris España](https://t.me/pastafarisespana) - Canal de noticias de Pastafaris en España
      * Tienen [grupo oficial](https://t.me/Pastafaris) - Grupo oficial para todos los Pastafaris en España
  * [Asociación República Pastafari de MonEsVol](https://t.me/asociacionpastafari) -
* Instituciones
  * [Ministerio de Sanidad](https://t.me/sanidadgob) - Canal oficial del Ministerio de Sanidad en Telegram - _Mayormete un clon de la cuenta de Twitter_
  * [Dirección General De Tráfico](https://t.me/DGT_ES) - Canal de la DGT. Información útil sobre seguridad vial y movilidad. Otras consultas en http://www.dgt.es
  * [Guardia Civil 🇪🇸](https://t.me/GuardiaCivil) - "El honor es mi principal divisa" - _No oficial_
  * [Policías de España](https://t.me/policias) - Información de utilidad para este cuerpo de seguridad. _No oficial_
  * [Ayuntamientos](https://amalialopezacera.com/listado-de-71-canales-de-telegram-de-ayuntamientos-activos) - Listado de 71 canales de Telegram de ayuntamientos (activos) por "El Blog de Amalia López Acera"
* Internacional
  * [DOUE diario](https://t.me/DOUEdiario) - El Diario Oficial de la Unión Europea (DO)
  * [ISIS Watch](https://t.me/ISISwatch) - This channel publishes daily updates on banned terrorist content. Report content via the in-app button or by emailing abuse@telegram.org - _(Oficial)_

### Regionales

#### Región de Murcia
* [Residuo Cero Region de Murcia](https://t.me/ResiduoCeroRM) - Información sobre noticias Zero Waste en la Región de Murcia
* [Ruta de la tapa vegana de Murcia 😋🥑 ](https://t.me/rutavegana) - Toda la información 🗞 sobre la Ruta de la tapa vegana de Murcia en tu mano
* [Cecilio Cean](https://t.me/ceciliocean) - Medio de comunicación social, solidario y altruista.
* [BORM diario](https://t.me/BORMdiario) - Titulares diarios (de lunes a sábado) del Boletín Oficial de la Región de Murcia.
* [112Lorca](https://t.me/Lorca112) - Servicio de Emergencias Municipal y Protección Civil de Lorca.
* [MC Hacking familiar](https://t.me/hackingfamiliar) - Trucos para hacer la vida más fácil a las familias. Salud, nutrición, viajes, cumpleaños, hogar, educación, seguridad infantil, gadgets...
* [Ubicaciones La Manga](https://t.me/UbiLaManga) - Ubicaciones La Manga `[Parado desde 2018]`
* [Greenpeace Murcia](https://t.me/GPMurcia) - GreenPeace Murcia - `[Sin contenido]`

#### Comunidad Valenciana
* [Feria Vegana Valencia](https://t.me/feriaveganavalencia) - http://feriaveganavalencia.org
* [GVA Educació](https://t.me/gvaEducacio) - Nou canal de comunicació per a la comunitat educativa valenciana
* [#CvTIC](https://t.me/cv_tic) - CvTIC. difusió de l'equip de coordinació TIC del Servei d'Informàtica per als Centres Educatius

#### Canarias
* [WiFi Canarias 📡](https://t.me/wificanarias) - WISP, Telecomunicaciones, Wireless, WiFi, \#WiFiConsejos y por supuesto: nuestros \#WiFiCanariasNews...

#### Madrid
* [LibreLabUCM](https://t.me/librelabucm) - [Asociación de estudiantes](http://librelabucm.org) de Tecnologías Libres en la UCM. ¡Noticias y actualidad del software libre\!
  * Tiene grupo: [LLU - LibreLabUCM](https://t.me/LLUchat) - Somos una asociación de estudiantes que apoya la tecnología libre, la cultura libre, el hacking y la seguridad... y nos gusta pasarlo en grande\!
* [XR Madrid](https://t.me/XRMadrid) - Canal de Extinction Rebellion Madrid. Entérate de todas las actividades del movimiento en la ciudad.💚✊🏽
* Universidad Rey Juan Carlos
  * [URJC](https://t.me/urjc_uni) - Canal oficial. Mantente al tanto de toda la actualidad universitaria:📰❗️ Noticias❗️📰 ⚽️🏀 Deportes 🏈⚾️...
  * [URJConline](https://t.me/URJConline) - Canal de URJC online (oficial). Información sobre la Universidad Rey Juan Carlos online.

#### Andalucía
* [#FridaysForFutureGRANADA](https://t.me/fridaysforfuturegranada) - Canal de información/difusión del movimiento juvenil y estudiantil #FridaysForFuture (Juventud por el Clima) en Granada.

#### Cataluña
* [gencat](https://t.me/gencat) - _Generalitat of Catalonia_
* [XR Barcelona](https://t.me/XRBarcelona) - Canal informatiu de Rebel·lió o Extinció Barcelona. Extinction Rebellion (XR) és un moviment internacional d'acció directa no violenta contra la inacció davant l'emergència climàtica
* [XR Girona](https://t.me/XRGirona) - Davant la crisi climàtica i ecològica global i la inacció de les elits polítiques i econòmiques ens rebel·lem!
* [AlertesUdG](https://t.me/alertesudg) - Avisos relacionats amb incidències que requereixin un avís immediat per a la comunitat universitària - _Universidad de Girona_

#### Castilla-La Mancha
* [Extinction Rebellion Toledo](https://t.me/XRToledo)


## GRUPOS

* [Coronavirus makers](https://t.me/coronavirus_makers) - _Grupo para colaborar en la creación de materiales para ayudar en la emergencia sanitaria_

### Software Libre

* [La Aldea de Tux 🐧 ](https://t.me/tuxaldea)
* [LinuxerOS](https://t.me/Linuxeros_es) - Ayuda sobre GNU/Linux, Consultas sobre Android en general, Deepweb, Hacking
* [Novato en Linux](https://t.me/novato_en_linux) - Dedicado a ayudar a aquellos que se inician en GNU/Linux
* [Hacking en Español](https://t.me/hackers_es)
* [Notxor tiene un blog](https://t.me/notxorblog) - _Emacs, org-mode..._
* [DriveMeca](https://t.me/drivemeca_opensource) - Grupo publico para discutir y compartir temas sobre opensource
* [OSM España](https://t.me/OSMes) - Grupo de contribuidores/editores de OpenStreetMap en/de España
  * [OpenStreetMap 🌐](https://t.me/OpenStreetMapOrg) - Global OpenStreetMap group.This is not a channel of official OSM communication, refer to IRC and mailinglist instead
* [Wikimedia General chat](https://t.me/WikimediaGeneral) - This group is public. In this group, the code of conduct for Wikimedia technical spaces is in effect
* [Clubdesoftwarelibre](https://t.me/CSLibre) - Un lugar donde vivimos la experiencia del Software Libre no sólo como código, sino como estilo de vida.
* [Karla's Project](https://t.me/KarlasProject) - Experiencias, opiniones y comentarios sobre todo aquello relacionado con la temática del canal: Windows, GNU/Linux, Informática
* Hispalinustálgicos _(Con invitación)_

### Aplicaciones

* [LibreOffice-ES](https://t.me/libreoffice_es)
* [Kdenlive en Español](https://t.me/kdenlive_es) - Comunidad en habla hispana de Kdenlive
* [Inkscape En Español](https://t.me/InkscapeES) - Grupo enfocado al diseño de ilustraciones y logos con el editor de gráficos vectoriales de código abierto.
  * Recomienda grupo [Arte, Dibujos, Ilustraciónes y Logos.](https://t.me/ArteGraficoLibre)
* [FreeCAD en Español](https://t.me/FreeCAD_Es) - Grupo de FreeCAD en Español. Todas tus ideas o proyectos son bienvenidos. Puedes vender tu servicio de diseño o consultar un problema e intentaremos ayudarte.
* [Comunidad Octoprint Esp](https://t.me/octoprint) - Comunidad Octoprint Español, donde intentaremos ayudarte en todo. Y solucionar tus problemas. + Método ESP8266
* [Node-red Spain](https://t.me/nodered)
* [Nextcloud](https://t.me/Nextcloud) - _English_
* [Mautrix-Telegram Bridge](https://t.me/mautrix_telegram) - A Matrix-Telegram hybrid puppeting/relaybot bridge

### Podcasts sin canal

* [Podcast "Actualidad y Empleo Ambiental"](https://t.me/podcastActualidadEmpleoAmbiental) - para comentar, debatir, proponer y todo lo que se nos ocurra\!
* Tecnología
  * [Entre Dev y Ops 🎙](https://t.me/entredevyops) - Comunidad del [podcast](https://www.entredevyops.es)
  * [Home Studio Libre](https://t.me/HomeStudioLibre) - Producción y postproducción de audio y vídeo con herramientas libres.
  * [Mixx.io y otros podcast](https://t.me/mixxiocomunidad)
  * [Yo Virtualizador](https://t.me/grupovirtualizador)
  * [ANDROYTECNO](https://t.me/PodcastAndroytecno) - Aquí se intentará hablar de Tecnología y sobre todo de dispositivos móviles.
  * [SystemInside](https://t.me/systeminsidegroup) - Nuestro idioma es La Tecnología. Aquí tiene cabida cualquier Sistema Operativo, Software y Hardware. Si, incluso hasta si es una tostadora.
* [La cocina está abierta](https://t.me/lacocinaestaabierta)
* [Esto No Es Politica](https://t.me/ETEPolitica)
* [Vacía tu bandeja, Podcast](https://t.me/Vaciatubandeja) - ...Sobre organización personal y creación de hábitos
  * Recomienda el grupo [Mi Bullet Journal](https://t.me/miBulletJournal)
* [Unión Podcastera](https://t.me/unionpod) - "Fraternidad de Podcasting” unionpodcastera.com / Conectamos el podcasting hispanoamericano
* [De Rodillas Grupo](https://t.me/derodillas) - Ni aunque me lo pidas de rodillas
* [El Peor Consultorio](https://t.me/elpeorconsultorio)
* [Pienso luego ya tu sabeh](https://t.me/piensoluegoya)
* [Series Reality Podcast](https://t.me/joinchat/AYDUWk73myOxZ7140NsJGg)
* Ciencia
  * [Bacteriófagos](https://t.me/Bacteriofagos) - Podcast quincenal de curiosidades biológicas y actualidad científica para todos los públicos. Presentado por Carmela García.
  * [Asociación Cultural Buhardillista](https://t.me/buhardillapodcast)- La Buhardilla 2.0 es un podcast sobre ciencia, tratada desde un punto de vista distinto a lo que estamos acostumbrados, pero cuyo fin es hacer que el oyente aprenda algo nuevo cada vez que acabe de escuchar un episodio.
* [Maratonpod](https://t.me/maratonpodchat) - Este es un grupo para que los oyentes estén al tanto de las novedades del MaratonPOD y puedan interactuar con los podcasters

### Bash / Shell

* [ShellDevs - Español](https://t.me/shelldevs_es) - Grupo en español para scripting developers. su consulta no molesta. Natanael
* [Bash / Zsh / CLI](https://t.me/BashZshCLI) - Grupo para tratar temas relacionados con BASH/Zsh y aplicaciones CLI

### Distribuciones GNU/Linux

* [Pásate a GNU/Linux](https://t.me/pasategnulinux) - Grupo de Telegram para solventar dudas sobre la instalación de GNU/Linux. Sólo se responde a dudas
* [GNU/LINUX](https://t.me/GnuLinuxGrupo) - distribuciones, ayuda, errores, drivers, instalaciones...
* [ShurLinuxeros Amantes de GNU/Linux](https://t.me/shurlinuxeros) - _Poco movimiento_
* DEB
  * [Debian_es](https://t.me/Debian_es) - Grupo sobre Debian de hispano hablantes.
    * [Devuan en Español](https://t.me/devuan_es) - Grupo [oficial](https://devuan.org) de Devuan en Español
  * [Ubuntu en Español](https://t.me/ubuntu_es) - Grupo dedicado a Ubuntu y sus derivadas
    * [Lubuntu Oficial en Español](https://t.me/lubuntues) - Lubuntu
    * [Ubuntizados](https://t.me/ubuntizados) - #Grupo colaborativos de ubuntizados. Usuari@s, seguidor@s, admirador@s y todos aquell@s que tenga interés por la distribución Ubuntu en sus diferentes sabores, Ubuntu, Kubuntu, Xubuntu, Lubuntu,
    * [KDE neon ES](https://t.me/kdeneones) - Grupo de habla hispana para entusiastas de KDE neon (no oficial)
    * [Kubuntu en Español](https://t.me/Kubuntues) - Grupo NO oficial de Kubuntu en Español
    * [Kubuntu Support](https://t.me/kubuntu_support) - Official Kubuntu support - _Oficial_
    * [Ubuntu Studio en Español](https://t.me/UbuntuStudio) - Un grupo creado para ayudarnos a usar este lindo sistema para humanos creativos, tambien compartir nuestros conocimientos y creaciones.
    * [Linux Mint en Español](https://t.me/linuxmint_es) - Grupo oficial de Linux Mint en Español
  * [DeepinOS](https://t.me/deepin_es) - Grupo Oficial de DeepinOS en Español!
    * [Deepin en Español](https://t.me/deepinenespanol) - Somos un grupo en español sobre Deepin
  * [Huayra GNU/Linux](https://t.me/huayra_comunidad) - Grupo Oficial de la Comunidad [Huayra Linux](https://huayra.conectarigualdad.gob.ar/) - _desarrollado por Educ.ar SE para los Programas Conectar Igualdad y Primaria Digital._
* RPM
  * [Fedora en Español](https://t.me/fedora_es) - Grupo oficial de Fedora en Español.
  * [CentOS en Español](https://t.me/centos_es) - Grupo oficial de CentOS en Español
  * [Red Hat Linux en Español](https://t.me/redhat_es) - Grupo oficial de Red Hat en Español
* Rolling Release
  * [openSUSE Soporte](https://t.me/opensusesp) - Soporte para usuarios de [opensuse](https://en.opensuse.org/openSUSE:Telegram) Linux en Español
    * [OpenSUSE en Español](https://t.me/opensuselinux) - Grupo “~~oficial~~” de OpenSUSE en Español - _(Poco movimiento)_
  * Arch based
    * [Arch Linux y Derivadas](https://t.me/ArchLinuxYDerivadas) - Arch Linux y distribuciones derivadas. Creado para ayudar y dar soporte a los usuarios de tales distros
    * [Arch Linux](https://t.me/archlinuxgroup) - Unofficial group for discussing everything about Arch Linux.
    * [Anarchy Linux en Español](https://t.me/anarchyenespanol) - Grupo de usuarios de [Anarchy](https://anarchylinux.org) Linux
    * [Manjaro en Español](https://t.me/manjarolinuxes) - No Oficial de Manjaro GNU/Linux. Con Calidad de Oficial.
    * [Antergos en Español](https://t.me/antergosesp) y [Antergos en Español](https://t.me/antergos_es) - _(Distro descontinuada)_
    * [Antergos (Archive)](https://t.me/Antergos) - _(En inglés)_
  * [Void Linux en Español](https://t.me/voidlinux_es) - Grupo oficial de Void Linux en Español
  * [Gentoo Linux](https://t.me/gentoo_es) - Grupo oficial de Gentoo en español!
  * [Kali Linux](https://t.me/kalilinux_es) - Grupo oficial de Kali Linux en Español.
* Otras
  * [Slackware en Español](https://t.me/slackware_es) - Grupo oficial de Slackware Linux en Español
  * [Alpine Linux Espanol](https://t.me/alpine_linux_espanol) - Linux en serio!. efectivo rapido.. incompatible.. como todo lo que hace un hacker hoy dia!
* [Raspberry Pi](https://t.me/GrupoRaspberryPi) - Grupo general para hablar sobre Raspberry Pi. Tenemos grupos sobre temáticas específicas como emuladores, mediacenters _Moderado por @vlcguardianbot_
  * [NextCloudPi](https://t.me/NextCloudPi) - Nextcloud instance that is preinstalled and preconfigured

#### Entornos de Escritorio

* KDE Plasma
  * [KDE en español](https://t.me/KDE_esp) - Grupo hecho para usuarios de KDE en cualquier distro.
  * [KDE - Cañas y Bravas](https://t.me/kde_canasbravas) - Únete a desarrolladores y usuarios KDE de todo el mundo para discutir tanto nuestro software como el mundo del software libre
  * [KDE Web](https://t.me/KDEWeb) - Managing KDE websites
* [Gnomeros](https://t.me/gnomeros) - Grupo dedicado al entorno de Escritorio por excelencia en GNU/Linux
* [Grupo XFCE](https://t.me/grupoxfce)

### Juegos

* [ENLinuxJugamos](https://t.me/EnLinuxjugamos) - Grupo para los que gustan de juegos en Linux
* [JugandoEnLinux.com en Español](https://t.me/jugandoenlinux) - Grupo de la [página](http://www.jugandoenlinux.com)
* [Todo Sobre Juegos Telegram](https://t.me/JuegosTg) - Grupo para jugar a los juegos de Telegram.

### Android / Moviles

* Android
  * [Club de Android y Gnu/Linux](https://t.me/ClubdeAndroidyGnuLinux) - Dos espacios reunidos en un mismo lugar, para usuarios de Android y Gnu/Linux. Además de  asesorias y ayuda en: gadgets, hardware, software, y demás S.O.
  * [andOTP](https://t.me/andOTP) - Official Telegram group for the open-source two-factor authentication app andOTP
* UBports
  * [UBports \[ESPAÑOL\]](https://t.me/UBPorts_ES) - Grupo oficial de la comunidad de UBPorts en Español
  * [UBports \[ENGLISH\]](https://t.me/ubports) - UBports [ENGLISH]
    * [UB Welcome & Install](https://t.me/WelcomePlus) - Ubports newcomers room - _(Para novatos)_

### Tecnológicos

* Telegram
  * [Todo Sobre Telegram](https://t.me/TodoSobreTelegram/136492) - _(Grupo sobre actualidad de todo lo relacionado con Telegram)_
  * [Todo Sobre Temas Telegram](https://t.me/TodoSobreTemasTelegram) - Grupo dedicado a los temas de Telegram.
  * [Geeks Chat](https://t.me/geeksChat) - Meeting point to talk about: ✳️ Telegram, News and Bots
* Programación/Formación
  * [Formadores Informática](https://t.me/Foentire) - Punto de encuentro para formadores de informática, donde contar nuestras experiencia, resolver dudas y cualquier tema relacionado con la formación.
  * [Python para docentes](https://t.me/Pythonparadocentes) - Grupo de discusión sobre Python en la educación (España)
  * [Python España](https://t.me/pythonesp) - Grupo de discusión sobre el lenguaje Python de la comunidad española
  * [CodeIgniter en Español](https://t.me/CodeIgniterEnEspanol) - Grupo de profesionales para compartir conocimientos sobre CodeIgniter y todo lo relacionado en programación y brindar asesorías entre todos.
* Sistemas
  * [Dockerlogy](https://t.me/dockerlogy) - Grupo para aprender a instalar y gestionar aplicaciones mediante docker
  * [Sysadmin - ES](https://t.me/Sysadmin_esp) - charla entre Sysadmins, para aprender los unos de los otros - _Mucho tráfico/mensajes_
  * [🐧SysAdmins de Cuba🇨🇺](https://t.me/sysadmincuba) - Consulta, Ayuda, Instalacion sobre cualquier tema referente a Redes en GNU/Linux, Windows y FreeBSD - _Muchísimo tráfico/mensajes_
* Seguridad
  * [Hackers en Español](https://t.me/hackers_es) - Grupo de Hacking en Español
  * [SEGURIDAD INFORMATICA](https://t.me/seginformatica) - Grupo de seguridad informática. Aquí estamos para aprender seguridad entre todos.
  * [InfoSec - es](https://t.me/infoseces) - Pregunta dudas técnicas, ayuda en las que puedas. Nada de política ni opinión. Es un canal de 1 y 0s
  * [Palo Alto Firewall / Panorama](https://t.me/paloalto_firewall) - Palo Alto Firewall / Panorama
* Compra/Venta
  * [Segunda Mano Informática| ForoSegundaMano.com](https://t.me/forosegundamano) - Las ofertas y detalles se preguntan a los vendedores por Privado. País: España 🇪🇸.
  * [RetroMercadillo](https://t.me/retromercadillo) - Grupo RetroMercadillo de compra-venta de electrónica y software retro de segunda mano
* Makers
  * [Making Cosillas ❤️](https://t.me/makingcosillas) - "Activity log" de Makerio: Poned acá cualquier actividad, evento o tarea de proyecto que estéis haciendo. _Tiene listado de grupos regionales_
  * [Spainlabs: CNC's](https://t.me/SpainLabsCNC) - Tu comunidad maker sobre CNC's, Impresión 3D, Arduino, Electrónica y mucho más.
  * [Comunicaciones LoRa / LoRaWAN](https://t.me/LoRa_LoRaWAN) - Comunicaciones LoRa / LoRaWAN
* Varios
  * [deDrones](https://t.me/dedrones) - charla y soporte sobre todo tipo de drones 😁
  * [Geeetech 🇪🇸 Esp A10 /A10M /20 /30](https://t.me/GeeetechSpain) - Grupo de impresión 3d de habla hispana

### Por Regiones

* Comunidad de Murcia
  * [Cultura y Software Libre de la Region de Murcia](https://t.me/MurciaRegionLibre) - Grupo de usuarios de software y cultura libre en la Región de Murcia
  * [Hack&Sec Murcia](https://t.me/hackandsecmurcia) - Este es un espacio de crecimiento personal para todo aquel que quiera aprender seguridad informática, donde todos podamos compartir conocimientos y poco a poco crear una comunidad en Murcia de gente con los mismos intereses
  * Grupo privado de "Cartagena News" de CartagenaNews con noticias sobre la ciudad - _(Cartagena.España)_
  * Por localización
    * Cartagena
      * Cartagena Residuo Cero / ZeroWaste
      * Cartagena Software Libre
    * Murcia
      * Residuo Cero Murcia♻️🌲
      * Software y Cultura Libre Murcia 🐧♻️
      * Go Vegan Murcia 🌱
* Comunidad Valenciana
  * [GNU/Linux Valencia](https://t.me/gnulinuxvalencia) - [Difusión de GNU/Linux.](https://gnulinuxvalencia.org) Se pretenden hacer reuniones mensuales para realizar desde instalaciones en equipos, resolver problemas, mostrar facilidad de uso, funcionamiento, etc... - _(español y valenciano)_
  * [Bitup Alicante Oficial](https://t.me/bitupalicante) - Comunidad y evento de ciberseguridad en Alicante (España)
* Andalucía
  * [Almería Trending](https://t.me/AlmeriaTrending) - Rincones, gastronomía, cultura, ocio, tendencias y sobre todo gente de Almería.
  * [LibreLabGRX](https://t.me/LibreLabGRX) - Grupo para la difusión y el apoyo al software/hardware libre y la cultura abierta desde Granada.
  * [Aula de Software Libre UCO](https://t.me/aulasoftwarelibreuco) - Grupo del Aula de Software Libre _de la Universidad de Córdoba_
* Galicia
  * [Galpon](https://t.me/galpon) - Grupo de Amigos de Linux de Pontevedra
* Cataluña
  * [Col.lectiu Ronda](https://t.me/collectiuronda) - Cooperativa d'assessorament jurídic, laboral i social
* Islas Baleares
  * [XR Ibiza Global Collaborators](https://t.me/joinchat/C5cQDxPVIPB_3rhhqEgUig) - For English speaking people that like to collaborate with XR Ibiza

### Grupos varios

* [PADELZOOM 🎾 Grupo Oficial](https://t.me/Padelzoom) - Grupo para hablar de una de nuestras mayores pasiones: las palas de pádel.
* [Todo Sobre AIRSOFT](https://t.me/TodoSobreAIRSOFT) - Grupo de jugadores de AIRSOFT para jugadores de AIRSOFT.
* [Komun.org (Castellano)](https://t.me/komun_es) - Herramientas para seres socialmente iguales, humanamente diferentes y totalmente libres.
  * Tiene también [canal](https://t.me/komun)
* [El futuro del periodismo](https://t.me/joinchat/CbJCaU1ZtNvHhIpxsmD-ag) - _Debate de noticias sobre periodismo y redes sociales_
* XR (Extinción/Rebelión)
  * [Rebel Beyond borders:COP25-MADRID](https://t.me/joinchat/AAAAAFaCd4MgsbSV_bg_bQ) - Grupo de Telegram XR durante COP25
  * [Mobilissation Station](https://t.me/joinchat/NZUJVRQ3l6joyfXekMf-ig) - XR Mass Mobilisation group

## BOTs
`Usar bots tiene sus consideraciones de privacidad, probarlos teniendo en cuenta el contenido que se les manda`

* @Politi_bot - Piezas informativas e infografías de manera interactiva
* @bulobot - Para enviar bulos a los de [Maldito Bulo](https://t.me/malditobulo)
* @bulo_blocker_bot - Para mandarle enlaces de noticias medioambientales (URLs) con el comando /bulo y GreenPeace las investiga.
* @eltiempoBot - Predicciones meteorológicas y notificaciones diarias para los municipios de España.
* @aemetbot - Consulta el tiempo de tu localidad. Sólo España.
* [@wwwhatsnew](https://t.me/Wwwhatsbot) - Muestra de forma automática las noticias publicadas en WWWhatsnew

### Encuestas

* @ultimate_pollbot - [Open Source](https://github.com/Nukesor/ultimate-poll-bot) - _Crea encuestas de todo tipo_
* @groupagreebot - _Crea encuestas de todo tipo_ - `[Offline]`
  * @advancedpollbot - _Crea encuestas de todo tipo y permite añadir opciones en caliente_

### Gestión canales / RRSS

* @ControllerBot - Awesome bot for channel owners that helps you to create rich posts, view stats and more. _(Permite programar, añadir botones (texto/emojis, comentarios..), estadísticas...)_
* @discussbot - Make this bot an admin in your channel to add comment buttons to all posts. _(Oficial)_
* @CommentsBot - I can help you to create posts with comments, share them to your channels or groups.
* @IFTTT - Link your Telegram groups or channels to more than 360 other services. - _(Oficial de IFFT)_
* @GramToolBot - Instagram helper bot on Telegram!. I can assist you with all sorts of things such as downloading posts, getting account stats, reseaching hashtags, finding viral content and much more!

### Administración de Grupos

* [@join_captcha_bot](https://t.me/join_captcha_bot) - [Open Source](https://github.com/J-Rios/TLG_JoinCaptchaBot) - To verify if a new user in a group is human - _AntiSpam/bots en el login para grupos_
* [Shieldy](https://t.me/shieldy_bot) - [Open Source](https://github.com/backmeupplz/shieldy) - _AntiSpam/bots en el login para grupos_
* [@DistrIUsebot](https://t.me/distriusebot) - [Open Source](https://gitlab.com/schcriher/distriusebot) - creado para listar las distribuciones de GNU/Linux que usan los miembros de grupos de telegram. - _Ademas saluda y despide a los usuarios_
* [@GroupButler_bot](https://t.me/GroupButler_bot) - [Open Source](https://github.com/group-butler/GroupButler) - managing your group with rules, anti-flood, description, custom trigger - _Sin probar_
* [@combot](https://t.me/combot) - I gather chats stats, visualize them and help you to engage your community. / [Manage:](combot.org/u/login) - _Sin probar_
* [@InviteMemberBot](https://t.me/InviteMemberBot) - membership bot platform for paid Telegram channels and groups
* @MissRose_bot - A group admin bot to help you manage your groups - _(es privativo por cabreo del developer)_ - [Rose Support](https://t.me/RoseSupport)

### Para encontrar en Telegram

* [@tchannelsbot](https://t.me/tchannelsbot) - Discover the best channels 📢 available on Telegram. Explore charts, rate ⭐️ and enjoy updates\! TChannels.me
* [@StoreBot](https://t.me/StoreBot) - Helps you discover the best bots 👾 on Telegram. Explore charts, rate bots and enjoy updates\! StoreBot.me
* [Telegram Bot Store (StoreOfBot)](https://t.me/StoreOfBot) - Search, Explore & Discover the best bots or channel

### Develop

* @HttpResponseBot - With this Bot you can get the HTTP status code and the redirects instantly for any website. Send me a link.
* @GithubReleasesBot - A chatbot that notify the user by telegram's message for a followed Github repository's new releases
* [@GitHubBot](https://t.me/githubbot) - Get notifications about events in your public GitHub repositories and post comments directly from Telegram.
* [@github_gist_bot](https://t.me/github_gist_bot) - Bot uploads text and documents to GitHub Gist.
* [@MiddlemanBot](https://t.me/MiddlemanBot) - [Open Source](https://github.com/n1try/telegram-middleman-bot) - Message broker bot to translate HTTP calls into Telegram messages.

### Aprobados por Telegram

* [@telegraph](https://t.me/telegraph) - To manage your telegra.ph publications and log in across any number of devices _(Oficial)_
* [@BotFather](https://t.me/botfather) - BotFather is the one bot to rule them all. Use it to create new bot accounts and manage your existing bots.
* [@like](https://t.me/like) - _(Mensajes con botones)_
* [@Stickers](https://t.me/Stickers) - Create Telegram Stickers and get usage stats for your stickers with this bot. - _Tambien inline_
* [@vote](https://t.me/vote) - _Creador de encuestas. El mismo integrado en los 3 puntos de arriba a la derecha._
* [@QuizBot](https://t.me/quizbot) - _multi-question quizzes and share them with others_
* [@GmailBot](https://t.me/GmailBot) - With this bot you can get new emails and reply to them without leaving Telegram.[Privacy](https://telegram.org/privacy/gmailbot)
* Busquedas inline
  * [@gif](https://t.me/gif) - Type @gif with a search query in your channel and gif suggestions will show up
  * [@pic](https://t.me/pic) - _(imagenes sacadas de Yandex)_
  * @youtube - _(Busca videos)_
    * [@vid](https://t.me/vid) - Official YouTube videos.
  * @sticker - enter and emoji
  * [@imdb](https://t.me/imdb) - Official Movies from IMDB.
  * [@bold](https://t.me/Bold) - Markdown bot / Highlight your message with bold, italic or fixed width.
  * [@gamee](https://t.me/gamee) - Official telegram bot for HTML5 gaming of [Gamee](https://www.gamee.com/) platform. - _Juegos dentro de Telegram_
  * [@music](https://t.me/music) - Official Classic music search. - _Música Clásica_
  * @wiki - _Wikipedia_
  * @bing - _Imagenes de bing_
* Soporte
  * @NoToScam - Reports about scammers are welcome here.
  * @GDPRbot - Telegram's Official GDPR bot. _(Para descargar los datos que tiene Telegram sobre ti. Oficial)_
  * @Username_bot - Claim an occupied username for your Telegram account, public group or channel. - _(Solicitar que liberen un alias)_
  * [@VerifyBot](https://t.me/VerifyBot) - This bot can help you verify a Telegram channel, public group or bot. - _Bot para verificar cuentas_
  * @TelegramAuditions - _(Para solicitar colaborar con Telegram. Oficial)_
  * [@BotSupport](https://t.me/botsupport) - _(Para mandarles sugerencias sobre los bots. Oficial)_
  * [@SpamBot](https://t.me/SpamBot) - The official Spam Info Bot by Telegram. Helps users with limited accounts regain the full functionality.
  * [@previews](https://t.me/Previews) - Use this bot to leave feedback about webpage previews generated for Telegram's Instant View feature.
  * [@jobs_bot](https://t.me/jobs_bot) - This bot lists career opportunities at Telegram and accepts candidates' applications. Available at telegram.org/jobs

### Utilidades

* @MiTrackingBot - Este bot permite llevar un registro de tus trackings con notificaciones automáticas
* @cuandosale_bot - Para saber la fecha de salida de un capítulo
* [@reverseSearchBot](https://t.me/reverseSearchBot) - [Open Source](https://github.com/kawaiiDango/reverseSearchBot) - Find the source of an image/GIF/sticker or identify the anime character, show, episode & time from its screenshot.
* [@transcriber_bot](https://t.me/transcriber_bot) - [Open Source](https://github.com/charslab/TranscriberBot) - Voz a texto
* [@voicybot](https://t.me/voicybot) - [Open Source](https://github.com/backmeupplz/voicy) - bot for translating voice recordings into text (speech-to-text) - _Sin probar_
* [@ExpenseBot](https://t.me/ExpenseBot) - [Open Source](https://github.com/n1try/telegram-expense-bot) - Bot for managing your daily financial expenses.
* [@randymbot](https://t.me/randymbot) - [Open Source](https://github.com/backmeupplz/randymbot) - Randy can randomly select a raffle participant in a chat or a channel
* [GiveawaysBot 🎰](https://t.me/Giveaways_Bot) - [Open Source](https://github.com/DanySpin97/GiveawaysBot)`2017` - handles giveaways creation and management, allowing users to easily create, join and share giveaways.
* [@TheFeedReaderBot](https://t.me/TheFeedReaderBot) - The Feed Reader Bot monitors RSS feeds (and Twitter accounts), and sends messages when new posts are available.
* [@yourcastbot](https://t.me/yourcastbot) - The podcast bot. Search, subscribe and listen podcasts. - _Sirve para buscar, escuchar y bajar episodios de Podcasts_
* [@pronunciationbot](https://t.me/pronunciationbot) - This is a bot that allows you to convert text into speech in 84 languages. - _Texto a voz_
* [@VisionTest1Bot](https://t.me/visiontest1bot) - Text description of photos - _Le mandas una foto y te intenta decir lo que hay en ella._
* [@qr_scanner_bot](https://t.me/qr_scanner_bot) - [Open Source](https://github.com/Helias/QR-Scanner-Bot) - This bot decode a qrcode.
* [@codescanbot](https://t.me/codescanbot) - 🕵️‍ Scan a QR or Barcode (multiple formats)  / ⚙️ Generate a QR Code image
* [@covid19_spa_bot](https://t.me/covid19_spa_bot) - [Open Source](https://github.com/JuanjoSalvador/covid19-reporter-bot) - COVID-19 Spain Reporter - Manager for @covid19_spa
* [InstantViewBot](https://t.me/CorsaBot) - [Open Source](https://github.com/albertincx/formatbot1) - Make Instant View from any article, chat
* [@scihubot](https://t.me/scihubot) - _Para mandarle conseguir articulos cientificos_
* [@Is_it_UPBot](https://t.me/Is_it_UPBot) - [Open Source](https://github.com/WBerredo/IsItUP) - Bot to verify if a website is up or track an URL to be notified when it gets down. 
* [@watch_bot](https://t.me/@watch_bot) - _Un sencillo bot para monitorizar servicios web y estar alerta de posibles caidas_
* Inline
  * [@OSMbot](https://telegram.me/OSMbot) - Search in OpenStreetMap database: addresses, maps, etc. Also in inline mode
  * @SpoilerzBot - _(Permite enviar texto o imagen como spoiler y no es legible en el grupo o privado sin interacción = inline)_
  * [@relevantxkcdbot](https://telegram.me/relevantxkcdbot) – Search XKCD comics.
  * [@asciifacesbot](https://telegram.me/asciifacesbot) – Bot that allows you to append ascii faces ¯\_(ツ)\_/¯ to your messages
  * [@archwikibot](https://t.me/archwikibot) - [Open Source](https://github.com/TheWeirdDev/arch-wiki-bot) - Inline search through ArchWiki pages in telegram!
  * [@lmddgtfybot](https://t.me/lmddgtfybot) - Let Me DuckDuckGo That For You / Send an inline query to @lmddgtfybot and I will provide you the best sites to troll your lazy friend. - _(Tambien Wikipedia, Google y Bing)_
  * [@raebot](https://t.me/raebot) - Bot no oficial para consultar definiciones de palabras según el diccionario de la Real Academia Española.
    * [@drae_bot](https://t.me/drae_bot) - Diccionario RAE (No Oficial)
* Buscadores de letras de canciones
  * @song_lyrics_bot
  * @iLyricsBot - _También inline_
  * @BingMusicBot - _También vídeos_
  * @LyricsGramBot


## RECURSOS

### Oficiales
* [Código fuente](https://telegram.org/apps#source-code) - For the moment we are focusing on open sourcing the things that allow developers to quickly build something using our API.
* [Palabra a emoji](https://translations.telegram.org/es/emoji) - _Traducción de Telegram, en la que pones una palabra o simbolo y sugiere un emoji_
* [Authorization](https://my.telegram.org/auth) - Delete Account or Manage Apps

### Utilidades
* https://telesco.pe/ - ***Oficial*** - _Muestra una web solo con los videos circulares de un canal_
* https://т.website/ - _Generador de previews insertables_

### IFTTT
* Tiempo / Meteorología
  * [WU a TG_Chat](https://ifttt.com/applets/ah7SPBUe-send-a-message-to-a-telegram-chat-if-there-will-be-rain-in-your-area-tomorrow) - Send a message to a Telegram chat if there will be rain in your area tomorrow - _Weather Underground_
  * [WU a TG_Chat](https://ifttt.com/applets/V5jg4iNb-its-gonna-rain) - Sends you a telegram message if its going to rain - _Weather Underground_
* Facebook
  * [Fotos a TG_Chat](https://ifttt.com/applets/urbQAevJ-save-photos-you-re-tagged-in-on-facebook-to-a-telegram-chat) - Save photos you're tagged in on Facebook to a Telegram chat
* Twitter
  * Telegram -> TW
    * [Canal a Tweet](https://ifttt.com/applets/FyU3EhJZ-tweet-new-text-posts-from-your-telegram-channel) - Tweet new text posts from your Telegram channel
    * [Grupo a Tweet](https://ifttt.com/applets/bZca3fjt-telegram-to-twitter-posting) - When posted on telegram group, also
posts tweet
    * [Canal a Tweet](https://ifttt.com/applets/F83hWUC2-export-telegram-channel-posts-to-twitter) - Export telegram channel posts to twitter. W/O double info in tweet
    * [Canal a Tweet](https://ifttt.com/applets/zywpdZzv-auto-twit-from-telegram) - Auto post from your telegram channel into twitter
  * TW -> Telegram
    * [@mencion a TG_Chat](https://ifttt.com/applets/UjfzrX8Y) - Whenever you are mentioned on Twitter, post a message to Telegram
    * [@Usuario TW a Chat](https://ifttt.com/applets/nxWRgmZr-keep-up-with-a-user-s-tweets-on-telegram) - When a specific user Tweets, this Applet will
send it to Telegram
    * [@Usuario TW a Chat](https://ifttt.com/applets/cgMtzsY6-forward-user-tweets-to-telegram) - Forward user tweets to Telegram
    * [@Usuario TW con $PATRON a Chat](https://ifttt.com/applets/MvW6xs5n-telegram-me-if-tweet-by-specific-user-with-specific-word-s) - Telegram me if tweet by specific user with specific word(s)
    * [Feed a Chat](https://ifttt.com/applets/NBYc6xbq-forward-twitter-feed-to-telegram) - Forward twitter feed to telegram
* Instagram
  * IG -> Telegram
    * [Foto a TG_Chat](https://ifttt.com/applets/YCB6Gvfg-every-time-you-post-a-new-instagram-photo-share-it-to-a-telegram-chat) - Every time you post a new Instagram photo, share it to a Telegram chat
    * [Video con # a TG_Chat](https://ifttt.com/applets/yhdqYTXw-post-your-new-instagram-videos-with-a-specific-hashtag-to-a-telegram-chat) - Post your new Instagram videos with a specific hashtag to a Telegram chat
    * [URL post a TG_G-Chat](https://ifttt.com/applets/Zu8X9dDz-send-my-instagram-post-url-to-telegram-group-chat) - Send my Instagram Post URL to Telegram Group Chat
    * [Foto a TG_Chat](https://ifttt.com/applets/D6SJMnAm-post-your-instagram-photos-to-telegram) - Post your Instagram photos to telegram
    * [Foto con # a TG_Chat](https://ifttt.com/applets/xj4WGzXn-post-instagram-photos-with-a-specific-hashtag-to-a-telegram-chat) - Post Instagram photos with a specific hashtag to a Telegram chat
    * [Foto con # a TG_Canal](https://ifttt.com/applets/KEGdKQyw-post-my-instagram-photos-to-telegram-channel) - Post my Instagram photos to Telegram Channel
    * [Foto a TG](https://ifttt.com/applets/D3kgmict-post-your-instagram-pics-to-your-telegram-chat-or-channel) - Post your Instagram pics to your Telegram chat or channel!

### Para bots
* Info sobre crearlos
  * [Awesome Telegram Bots](https://github.com/DenisIzmaylov/awesome-telegram-bots#readme) -  Open Source Examples, Libraries and Starter Kits for Telegram Bots to speed up your learning process.
  * [Awesome Bots](https://github.com/abdelhai/awesome-bots#readme) - A curated list of links and resources about bots. - _Bots en general no especifico de Telegram_
* Buscadores:
  * [BotoStore](https://botostore.com/bots/telegram/) - _Buscador de bots con categorías_  
  * [Telegram Bot List](https://botlist.net/bots/telegram) - _Buscador de bots con categorías_
* Listados
  * https://telegra.ph/Recomendaci%C3%B3n-de-BOTS-de-Androidsis-07-30-2 - Recomendación-de-BOTS-de-Androidsis-07-30-2
* Utilidades
  * [telegrun](https://gitlab.com/iMil/telegrun) - Execute local commands via Telegram messages

### Funcionamiento
* Tutoriales
  * [Así es TELEGRAM, la mejor alternativa a WHATSAPP | ChicaGeek](https://www.youtube.com/watch?v=l19qpCDvQb8) - Video con funcionamiento básico - 20 minutos
  * [Telegram Desktop. Atajos de teclado](https://ugeek.github.io/blog/post/2020-02-28-telegram-desktop-atajos-de-teclado.html) - Por uGeek

### Para encontrar canales, grupos o bots
* Otros listados
  * [awesome-telegram-lists](https://github.com/lorien/awesome-telegram-lists) - _Listado de listas con recursos de Telegram_
  * [awesome-telegram](https://github.com/ebertti/awesome-telegram#readme) - A curated list of Telegram resources - _En inglés y ruso_
  * [Comunidades Tecnológicas de Venezuela en Telegram](https://github.com/OpenVE/comunidades-en-telegram)
  * https://canales.xyz/telegram/ - Grupos y canales de Telegram en español
* Buscadores
  * https://telegramchannels.me/ - Discover The Best Telegram Channels - _Channels, Groups, and Bots_
  * https://telegramic.org - Social Telegram Directory - _Channels, Groups, Bots and Stickers_
  * https://tdirectory.me/ - Discover Popular Telegram Channels, Groups, Bots and Games
  * https://tgram.io/ - Telegram groups list, telegram group chat, telegram chat rooms, telegram groups to join
  * Solo Canales
    * https://canalgram.com/directorio-canales/
  * Solo Grupos
    * http://www.grupostelegram.net/


## LEYENDA

* **Canal/Grupo/Bot** - Descripción dada por el autor/dueño - _(Comentario)_ - `[Estado parado o vacío]`


## Licencia

[Creative Commons Atribución-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es "CC-BY-SA")

[![Licencia Creative Commons](https://licensebuttons.net/l/by-sa/4.0/88x31.png "Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional")](https://creativecommons.org/licenses/by-sa/4.0)
